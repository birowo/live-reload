package main

import (
	"io"
	"log"
	"net/http"
	"os"
	"sync"

	"github.com/fsnotify/fsnotify"
)

func WatcherPool(path, name string) func(io.Writer, func(io.Writer) int64) {
	pool := sync.Pool{
		New: func() any {
			// Create new watcher.
			watcher, err := fsnotify.NewWatcher()
			if err != nil {
				log.Println(err)
				return nil
			}
			// Add a path.
			err = watcher.Add(path)
			if err != nil {
				log.Println(err)
				return nil
			}
			return watcher
		},
	}
	return func(w io.Writer, fn func(io.Writer) int64) {
		watcher_ := pool.Get()
		defer pool.Put(watcher_)
		watcher := watcher_.(*fsnotify.Watcher)
		// Start listening for events.
		var n int64
		for n == 0 {
			select {
			case evt, ok := <-watcher.Events:
				if ok && evt.Has(fsnotify.Write) && evt.Name == name[2:] {
					n = fn(w)
				}
			case err, ok := <-watcher.Errors:
				if ok {
					log.Println(err)
				}
			}
		}
	}
}
func FilePool(name string) func(io.Writer) int64 {
	pool := sync.Pool{
		New: func() any {
			file, err := os.Open(name)
			if err != nil {
				log.Println(err)
				return nil
			}
			return file
		},
	}
	return func(w io.Writer) (n int64) {
		file_ := pool.Get()
		defer pool.Put(file_)
		file := file_.(*os.File)
		file.Seek(0, 0)
		n, _ = io.Copy(w, file)
		return n
	}
}

func main() {
	const (
		path = "./watch"
		name = path + "/test.html"
	)
	var (
		filePool    = FilePool(name)
		watcherPool = WatcherPool(path, name)
	)
	http.HandleFunc("/ajax", func(w http.ResponseWriter, r *http.Request) {
		watcherPool(w, filePool)
	})
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		filePool(w)
	})
	http.ListenAndServe(":8080", nil)
}
